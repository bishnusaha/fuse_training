package com.training.common;

import javax.sql.DataSource;

import org.osgi.service.blueprint.container.BlueprintContainer;

public class CommonService {
	
	static BlueprintContainer blueprintContainer;
	
	static DataSource dataSource;

	public static BlueprintContainer getBlueprintContainer() {
		return CommonService.blueprintContainer;
	}

	public void setBlueprintContainer(BlueprintContainer blueprintContainer) {
		CommonService.blueprintContainer = blueprintContainer;		
		System.out.println("blueprintContainer ::"+blueprintContainer);
	}
	
	public static DataSource getDataSource()
	{
		DataSource dataSource = (DataSource) CommonService.blueprintContainer.getComponentInstance("myDS");
		return dataSource;
	}

}
