package com.training.processor;

import com.training.model.MyPayload;

public class MyBean {
	 
    public MyPayload createBody(String body) {
        return new MyPayload(body);
    }        
     
    public MyPayload addTwo(MyPayload body) {
        body.setValue(body.getValue() + " and two");
        return body;
    }
     
    public MyPayload addThree(MyPayload body) {
        body.setValue(body.getValue() + " and three");
        return body;
    }
}
