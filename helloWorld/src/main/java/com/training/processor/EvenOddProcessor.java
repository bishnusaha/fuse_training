package com.training.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class EvenOddProcessor implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		
		Integer body = exchange.getIn().getBody(Integer.class);
		if(body%2 == 0)
		{
			exchange.getIn().setHeader("even_odd", "even");
		}
		else
		{
			exchange.getIn().setHeader("even_odd", "odd");
		}

	}

}
