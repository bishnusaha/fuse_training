package com.training.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class CalculateDepartment implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
        String recipients = "direct:hr";        
        String body = exchange.getIn().getBody(String.class);
        String[] employee = body.split("\\|");
         if (employee[1].equals("new")) {
            recipients += ",direct:account,direct:manager";
            exchange.getIn().setBody(employee[0]+" Joined");
         } else if (employee[1].equals("resigns")) {
             recipients += ",direct:account";
             exchange.getIn().setBody(employee[0]+" Resigned");
         }
         exchange.getIn().setHeader("departments", recipients);
       }

}
