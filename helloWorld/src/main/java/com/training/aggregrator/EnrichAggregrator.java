package com.training.aggregrator;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

public class EnrichAggregrator implements AggregationStrategy {

	@Override
	public Exchange aggregate(Exchange orginal, Exchange resource) {		
		
		resource.getIn().setBody(orginal.getIn().getBody()+" "+resource.getIn().getBody());
		
		return resource;
	}

}
