package com.training.aggregrator;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

public class CustomAggregrator implements AggregationStrategy {

	@Override
	public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {		
		if(oldExchange != null)
		{
			newExchange.getIn().setBody(oldExchange.getIn().getBody()+"\n"+newExchange.getIn().getBody());
		}
		return newExchange;
	}

}
