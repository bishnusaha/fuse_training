Camel Project for Blueprint 
=========================================

To build this project use

    mvn install

To run the project you can execute the following Maven goal

    mvn camel:run

To deploy the project in OSGi. For example using Apache Karaf.
You can run the following command from its shell:

    osgi:install -s install mvn:com.example.integration/exceptionDemo/1.0.0-SNAPSHOT

For more help see the Apache Camel documentation

    http://camel.apache.org/
    
    
    blueprint.xml - having a route which is using doTry-doCatch where I throw an exception and it is handled.
    
    onExceptionContext.xml - having a route which is using onException with redeliveryPolicy where I throw an exception and it is handled.
    
    deadletterErrorContext.xml -  having a route which is using DeadletterError with redeliveryPolicy and DeadletterUri where I throw an exception and it is handled.
