package com.exception.demo.processor;

import org.apache.camel.Exchange;
import org.apache.camel.ExchangeTimedOutException;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExceptionProcessor implements Processor {
	
	Logger logger = LoggerFactory.getLogger(ExceptionProcessor.class);

	@Override
	public void process(Exchange exchange) throws Exception {
		
		Exception exception = exchange.getProperty(Exchange.EXCEPTION_CAUGHT,Exception.class);
		
		if(exception instanceof ExchangeTimedOutException)
		{	
			logger.info("Inside timeout excetion block");
			//write code to log the error in db.
			exchange.getIn().setHeader("exceptionType", "timeout");
			exchange.setException(null);
		}		

	}

}
