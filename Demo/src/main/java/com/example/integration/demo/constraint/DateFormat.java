package com.example.integration.demo.constraint;


import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.example.integration.demo.constraint.validator.DateFormatValidator;

@Target({java.lang.annotation.ElementType.FIELD, java.lang.annotation.ElementType.METHOD, java.lang.annotation.ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy={DateFormatValidator.class})
@Documented
public @interface DateFormat {
	
	String format();
	
	String message() default "{DateFormat.message}";
	  
	Class<?>[] groups() default {};
	  
	Class<? extends Payload>[] payload() default {};

}
