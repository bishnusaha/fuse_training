package com.example.integration.demo.model;

import java.io.Serializable;
import java.util.List;

public class Department implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private DeptHeader deptHeader;
	private List<Employee> employees;
	private DeptTrailer deptTrailer;
	
	public DeptHeader getDeptHeader() {
		return deptHeader;
	}
	public void setDeptHeader(DeptHeader deptHeader) {
		this.deptHeader = deptHeader;
	}
	public List<Employee> getEmployees() {
		return employees;
	}
	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}
	public DeptTrailer getDeptTrailer() {
		return deptTrailer;
	}
	public void setDeptTrailer(DeptTrailer deptTrailer) {
		this.deptTrailer = deptTrailer;
	}
}
