package com.example.integration.demo.processor;

import java.util.ArrayList;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.example.integration.demo.model.Department;

public class PropertyReadProcessor implements Processor {
	
	Logger logger = LoggerFactory.getLogger(PropertyReadProcessor.class);
	

	@Override
	public void process(Exchange beanIoExchange) throws Exception {
		
		logger.info("In Processor unmarshalBody property value ::"+beanIoExchange.getProperty("unmarshalBody"));
		
		ArrayList<Object> beanIoList = beanIoExchange.getIn().getBody(ArrayList.class);
		
		ArrayList<Department> departments = new ArrayList(beanIoList.subList(1, beanIoList.size() - 1));
		
		beanIoExchange.getOut().setBody(departments);
	}

}
