package com.example.integration.demo.processor;

import org.apache.camel.Exchange;
import com.javacodegeeks.ws.product_service.types.ProductRequest;
import org.apache.camel.Processor;

public class ProductRequestProcessor implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		ProductRequest productRequest = new ProductRequest();
		productRequest.setId(exchange.getIn().getBody(String.class));
		exchange.getIn().setBody(productRequest);
	}

}
