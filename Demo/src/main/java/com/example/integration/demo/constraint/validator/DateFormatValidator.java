package com.example.integration.demo.constraint.validator;



import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.example.integration.demo.constraint.DateFormat;


public class DateFormatValidator implements ConstraintValidator<DateFormat, String> {
	
	String format;

	@Override
	public void initialize(DateFormat constraintAnnotation) {
		this.format=constraintAnnotation.format();		
	}

	@Override
	public boolean isValid(String date, ConstraintValidatorContext context) {
		
		if ( date == null ) {
            return true;
        }

		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        dateFormat.setLenient(false);
        try {
            dateFormat.parse(date);
            return true;
        } 
        catch (ParseException e) {
        	context.disableDefaultConstraintViolation();
        	context.buildConstraintViolationWithTemplate(context.getDefaultConstraintMessageTemplate()).addConstraintViolation();
            return false;
        }
	}

}
