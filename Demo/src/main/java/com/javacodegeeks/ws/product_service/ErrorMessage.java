
package com.javacodegeeks.ws.product_service;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 3.4.1
 * 2020-12-01T05:34:27.659+05:30
 * Generated source version: 3.4.1
 */

@WebFault(name = "error", targetNamespace = "http://ws.javacodegeeks.com/product-service/types")
public class ErrorMessage extends Exception {

    private com.javacodegeeks.ws.product_service.types.Error faultInfo;

    public ErrorMessage() {
        super();
    }

    public ErrorMessage(String message) {
        super(message);
    }

    public ErrorMessage(String message, java.lang.Throwable cause) {
        super(message, cause);
    }

    public ErrorMessage(String message, com.javacodegeeks.ws.product_service.types.Error error) {
        super(message);
        this.faultInfo = error;
    }

    public ErrorMessage(String message, com.javacodegeeks.ws.product_service.types.Error error, java.lang.Throwable cause) {
        super(message, cause);
        this.faultInfo = error;
    }

    public com.javacodegeeks.ws.product_service.types.Error getFaultInfo() {
        return this.faultInfo;
    }
}
